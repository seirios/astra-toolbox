#include "astra/CompositeGeometryManager.h"
#include "cpp/creators.hpp"
#include "ctheader.h"

#include <iostream>
#include <omp.h>
#include <sys/time.h>
#include <string.h>

using namespace std;
using namespace astra;

inline static size_t dataSize(CFloat32Data3DMemory *data) {
    return (size_t)data->getWidth() * data->getHeight() * data->getDepth();
}

static void rotate_z(double angle, double x, double y, double z, double *x_o, double *y_o, double *z_o)
{
    *x_o = cos(angle) * x - sin(angle) * y;
    *y_o = sin(angle) * x + cos(angle) * y;
    *z_o = z;
}

// nx * ny * nz are the volume dimensions and (x0,y0,z0) is the volume center
CVolumeGeometry3D* createVolGeom(double voxel_size, int nx, int ny, int nz, double x0, double y0, double z0) {
    const double xmin = x0 - (voxel_size * nx) * 0.5;
    const double xmax = x0 + (voxel_size * nx) * 0.5;
    const double ymin = y0 - (voxel_size * ny) * 0.5;
    const double ymax = y0 + (voxel_size * ny) * 0.5;
    const double zmin = z0 - (voxel_size * nz) * 0.5;
    const double zmax = z0 + (voxel_size * nz) * 0.5;

    return create_vol_geom_3d(nx,ny,nz,xmin,xmax,ymin,ymax,zmin,zmax);
}

CConeVecProjectionGeometry3D* createConeProjGeom(const CT_FILE_HEADER *cthdr) {
    // sinogram dimensions
    const int nproj = cthdr->NProy; // angular resolution
    const int det_perp = cthdr->Pixels_X; // u resolution
    const int det_axial = cthdr->Pixels_Z; // v resolution
    const double pix = cthdr->PixelSize; // in mm

    // geometric calibration parameters
    const double R = cthdr->ctgeom.R; // in mm
    const double D = cthdr->ctgeom.focal; // in mm
    const double u_0 = cthdr->ctgeom.u_0; // in mm
    const double v_0 = cthdr->ctgeom.v_0; // in mm
    const double twist = cthdr->ctgeom.twist; // in degrees
    const double slant = cthdr->ctgeom.slant; // in degrees
    const double tilt  = cthdr->ctgeom.tilt;  // in degrees

    // From CT Calibration Bruker presentation
    // Tilt (rot. angle around x)
    // Slant (z)
    // Twist (y)
    // Where u <-> x, v <-> y, w <-> z
    // However, eta = twist & theta = slant (VERIFIED)
    // angles in radians
    const double eta   = twist * M_PI / 180.0; // in-plane angle
    const double theta = slant * M_PI / 180.0; // polar angle
    const double phi   = tilt  * M_PI / 180.0; // azimuthal angle

    // return value
    CConeVecProjectionGeometry3D *proj_geom;

    // workspace
    double e_w[3],e_u[3],e_v[3],alpha[3],beta[3];
    double src[3],det[3],pix_u[3],pix_v[3];
    float32 *angles;
    double *vectors;
    double gamma;
    int i;

    /* ===================================
       CONE BEAM GEOMETRY (Noo et al. '00)
       =================================== */

    // normal to detector plane
    e_w[0] = cos(theta) * cos(phi);
    e_w[1] = cos(theta) * sin(phi);
    e_w[2] = sin(theta);

    // x orthogonal direction from e_w
    alpha[0] = -sin(phi);
    alpha[1] =  cos(phi);
    alpha[2] =  0;

    // y orthogonal direction from e_w
    beta[0] = -sin(theta) * cos(phi);
    beta[1] = -sin(theta) * sin(phi);
    beta[2] =  cos(theta);

    // x direction along detector plane (including in-plane rotation)
    e_u[0] = cos(eta) * alpha[0] + sin(eta) * beta[0];
    e_u[1] = cos(eta) * alpha[1] + sin(eta) * beta[1];
    e_u[2] = cos(eta) * alpha[2] + sin(eta) * beta[2];

    // y direction along detector plane (including in-plane rotation)
    e_v[0] = cos(eta) * beta[0] - sin(eta) * alpha[0];
    e_v[1] = cos(eta) * beta[1] - sin(eta) * alpha[1];
    e_v[2] = cos(eta) * beta[2] - sin(eta) * alpha[2];

    // geometry at projection angle 0
    // cone vertex (source)
    src[0] = R;
    src[1] = 0;
    src[2] = 0;
    // vector from det pixel (0,0) to (1,0)
    // pix_u = pix * e_u
    pix_u[0] = pix * e_u[0];
    pix_u[1] = pix * e_u[1];
    pix_u[2] = pix * e_u[2];
    // vector from det pixel (0,0) to (0,1)
    // pix_v = pix * e_v
    pix_v[0] = pix * e_v[0];
    pix_v[1] = pix * e_v[1];
    pix_v[2] = pix * e_v[2];
    // center of detector (at a distance D from source, displaced by u_0/2 and v_0/2)
    // det = src    - D * e_w    + 0.5 * u_0 * e_u    + 0.5 * v_0 * e_v
    det[0] = src[0] - D * e_w[0] + 0.5 * u_0 * e_u[0] + 0.5 * v_0 * e_v[0];
    det[1] = src[1] - D * e_w[1] + 0.5 * u_0 * e_u[1] + 0.5 * v_0 * e_v[1];
    det[2] = src[2] - D * e_w[2] + 0.5 * u_0 * e_u[2] + 0.5 * v_0 * e_v[2];

    // rotate x -> -y (seems to work only this way!)
    gamma = -M_PI_2;
    rotate_z(gamma,src[0],src[1],src[2],&src[0],&src[1],&src[2]);
    rotate_z(gamma,det[0],det[1],det[2],&det[0],&det[1],&det[2]);
    rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&pix_u[0],&pix_u[1],&pix_u[2]);
    rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&pix_v[0],&pix_v[1],&pix_v[2]);

    // rotate by projection angle around Z
    angles = create_angles(0,2*M_PI,nproj);
    vectors = new double[12*nproj];
    for(i=0;i<nproj;i++) {
        gamma = angles[i];
        rotate_z(gamma,src[0],src[1],src[2],&vectors[12*i+0],&vectors[12*i+1],&vectors[12*i+2]);
        rotate_z(gamma,det[0],det[1],det[2],&vectors[12*i+3],&vectors[12*i+4],&vectors[12*i+5]);
        rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&vectors[12*i+6],&vectors[12*i+7],&vectors[12*i+8]);
        rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&vectors[12*i+9],&vectors[12*i+10],&vectors[12*i+11]);
    }
    delete[] angles;

    proj_geom = create_proj_geom_3d_cone_vec(det_axial,det_perp,nproj,vectors);
    delete[] vectors;

    return proj_geom;
}

int main (int argc, char *argv[])
{
    CCudaProjector3D *proy;
    CVolumeGeometry3D *vol_geom;
    CConeVecProjectionGeometry3D *proj_geom;
    CFloat32ProjectionData3DMemory *sinoData;
    CFloat32VolumeData3DMemory *volumeData;
    CCompositeGeometryManager *cgeomgr;
    struct timeval tp;
    double t0,t1,dt;

    CT_FILE_HEADER cthdr;
    unsigned int xres,yres,zres;
    double voxel_size; // in mm

    const char *file_cthdr,*file_sino,*str_geom;
    char *file_out;

    if(argc != 5) {
        fprintf(stderr,"[ERROR] Please supply CT header, sinogram, geometry and output filenames\n");
        exit(EXIT_FAILURE);
    }

    file_cthdr = argv[1];
    file_sino  = argv[2];
    str_geom   = argv[3];
    file_out   = strdup(argv[4]);

    // load CT header data
    if(ctheader_read(file_cthdr,&cthdr)) {
        fprintf(stderr,"[ERROR] Cannot read CT header: %s\n",file_cthdr);
        exit(EXIT_FAILURE);
    }

    // get dimensions from str_geom
    sscanf(str_geom,"%ux%ux%u@%lf",&xres,&yres,&zres,&voxel_size);

    // create volume geometry                         FOV center (mm)
    vol_geom = createVolGeom(voxel_size,xres,yres,zres,0.0,0.0,-2.0);

    // create cone-beam projection geometry
    proj_geom = createConeProjGeom(&cthdr);

    // create CUDA 3D projector
    proy = create_projector_3d_cuda(proj_geom,vol_geom);

    // create composite geometry manager
    cgeomgr = new CCompositeGeometryManager();

    // START init
    volumeData = create_data_3d_vol(vol_geom,0.0f);
    sinoData = load_data_3d_sino(file_sino,proj_geom);
    delete proj_geom;
    delete vol_geom;
    // END init

    // time at start (seconds)
    gettimeofday(&tp,NULL); t0 = tp.tv_sec + 1E-6 * tp.tv_usec;

    cgeomgr->doFDK(proy,volumeData,sinoData,false,NULL);

    // final time (seconds)
    gettimeofday(&tp,NULL); t1 = tp.tv_sec + 1E-6 * tp.tv_usec;

    delete sinoData;
    delete cgeomgr;
    delete proy;
    
    // save data
    save_data_3d(file_out,volumeData);
    delete volumeData;

    dt = t1 - t0;
    fprintf(stderr,"Total reconstruction time: %g%c\n",
            dt<60.0?dt:(dt<3600.0?dt/60.0:dt/3600.0),
            dt<60.0?'s':(dt<3600.0?'m':'h'));

    return 0;
}
