#ifndef ctheader_h_
#define ctheader_h_

#include <stdio.h>
#include <zlib.h>

struct CT_GEOM {
	double focal; 
	double R; 
	double u_0; 
	double v_0; 
	double twist; 
	double slant; 
	double tilt; 
};

#pragma pack(push)
#pragma pack(1)
typedef struct CT_HEADER
{ 
	unsigned short NProy; 
	unsigned short Pixels_X; 
	unsigned short Pixels_Z; 
	unsigned short Bits; 
	double PixelSize; 
    struct CT_GEOM ctgeom;
	unsigned short current; 
	unsigned short voltage; 
} CT_FILE_HEADER;
#pragma pack(pop)

int ctheader_read(const char *file, CT_FILE_HEADER *cthdr) {
    gzFile zfp;

    zfp = gzopen(file,"rb");
    if(zfp == NULL) return 1;
    if(gzfread(cthdr,sizeof(CT_FILE_HEADER),1,zfp) != 1) return 1;
    gzclose(zfp);

    return 0;
}

#endif
