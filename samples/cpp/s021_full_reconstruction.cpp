#include <cpp/creators.hpp>
#include <astra/CudaSirtAlgorithm3D.h>

#include <iostream>
#include <fstream>

using namespace std;
using namespace astra;

void rotate_z(double angle, double x, double y, double z, double *x_o, double *y_o, double *z_o)
{
    *x_o = cos(angle) * x - sin(angle) * y;
    *y_o = sin(angle) * x + cos(angle) * y;
    *z_o = z;
}

int main (int argc, char *argv[])
{
    int j,i,k;
    float32 *angles;
    float32 *sino_full_f;
    CVolumeGeometry3D *vol_geom;
    CConeVecProjectionGeometry3D *proj_geom;
    CCudaProjector3D *proj;
    CFloat32ProjectionData3DMemory *sino;
    CFloat32VolumeData3DMemory *rec;
    CCudaSirtAlgorithm3D *algo;
    double *vectors;
    ifstream file;
    long int offset;
    double gamma;
    double src[3],det[3],pix_u[3],pix_v[3];
    double e_w[3],e_u[3],e_v[3],alpha[3],beta[3];
    float32 norm;

    const double factor = 2; // projection downscaling factor
    const double pix = 0.1 * factor; // in mm
    const double R = 295.195; // in mm 
    const double D = 446.986; // in mm
    const double u_0 = -3.63942 / factor; // in mm
    const double v_0 = -1.35506 / factor; // in mm
    const double twist = -0.129224 * M_PI / 180.0; // in degrees?
    const double slant = -0.393957 * M_PI / 180.0; // in degrees?
    const double tilt  = -0.167292 * M_PI / 180.0; // in degrees?

    // sinogram dimensions
    const int nproj = 600;
    const int det_perp = 560;  // y resolution
    const int det_axial = 560; // z resolution
    const int sino_axial = det_axial; // axial subset

    // reconstruction geometry
    const double voxel_size = 0.065;//0.17; // in mm
    const int xres = 328; // limited by GPU memory
    const int yres = 328; // limited by GPU memory
    const int zres = 510; // limited by GPU memory
    // FOV center
    const double x0 = 0.0; // in mm
    const double y0 = -11.15; // in mm
    const double z0 = 1.73; // in mm
    // reconstruction limits
    const double xmin = x0 - (voxel_size * xres) * 0.5;
    const double xmax = x0 + (voxel_size * xres) * 0.5;
    const double ymin = y0 - (voxel_size * yres) * 0.5;
    const double ymax = y0 + (voxel_size * yres) * 0.5;
    const double zmin = z0 - (voxel_size * zres) * 0.5;
    const double zmax = z0 + (voxel_size * zres) * 0.5;

    // load sinogram data
    sino_full_f = new float32[det_perp*nproj*sino_axial];
    file.open("../../../../ct_sino_small.raw",ios::in|ios::binary);
    file.read((char*)sino_full_f,det_perp*nproj*sino_axial*sizeof(float32));
    file.close();

    // clear z borders of sinogram
    offset = 0;
    for(k=0;k<offset;k++)
        for(j=0;j<det_perp;j++)
            for(i=0;i<nproj;i++)
                sino_full_f[j + i*det_perp + k*det_perp*nproj] = 0;
    for(k=sino_axial-1;k>sino_axial-1-offset;k--)
        for(j=0;j<det_perp;j++)
            for(i=0;i<nproj;i++)
                sino_full_f[j + i*det_perp + k*det_perp*nproj] = 0;

    // create volume geometry
    vol_geom = create_vol_geom_3d(xres,yres,zres,xmin,xmax,ymin,ymax,zmin,zmax);

    /* ==================
       CONE BEAM GEOMETRY
       ================== */

    // normal to detector plane
    e_w[0] = cos(slant) * cos(tilt);
    e_w[1] = cos(slant) * sin(tilt);
    e_w[2] = sin(slant);

    // x orthogonal direction from e_w
    alpha[0] = -sin(slant);
    alpha[1] =  cos(slant);
    alpha[2] =  0;

    // y orthogonal direction from e_w
    beta[0] = -sin(tilt)*cos(slant);
    beta[1] = -sin(tilt)*sin(slant);
    beta[2] =  cos(tilt);

    // x direction along detector plane (including in-plane rotation)
    e_u[0] = cos(twist) * alpha[0] + sin(twist) * beta[0];
    e_u[1] = cos(twist) * alpha[1] + sin(twist) * beta[1];
    e_u[2] = cos(twist) * alpha[2] + sin(twist) * beta[2];

    // y direction along detector plane (including in-plane rotation)
    e_v[0] = cos(twist) * beta[0] - sin(twist) * alpha[0];
    e_v[1] = cos(twist) * beta[1] - sin(twist) * alpha[1];
    e_v[2] = cos(twist) * beta[2] - sin(twist) * alpha[2];

    // geometry at projection angle 0
    // cone vertex (source)
    src[0] = R;
    src[1] = 0;
    src[2] = 0;
    // vector from det pixel (0,0) to (1,0)
    // pix_u = pix * e_u
    pix_u[0] = pix * e_u[0];
    pix_u[1] = pix * e_u[1];
    pix_u[2] = pix * e_u[2];
    // vector from det pixel (0,0) to (0,1)
    // pix_v = pix * e_v
    pix_v[0] = pix * e_v[0];
    pix_v[1] = pix * e_v[1];
    pix_v[2] = pix * e_v[2];
    // center of detector (at a distance D from source, displaced by u_0 and v_0)
    // det = src    - D * e_w    + u_0 * e_u    + v_0 * e_v
    det[0] = src[0] - D * e_w[0] + u_0 * e_u[0] + v_0 * e_v[0];
    det[1] = src[1] - D * e_w[1] + u_0 * e_u[1] + v_0 * e_v[1];
    det[2] = src[2] - D * e_w[2] + u_0 * e_u[2] + v_0 * e_v[2];

    // rotate x -> -y (seems to work only this way!)
    gamma = -M_PI_2;
    rotate_z(gamma,src[0],src[1],src[2],&src[0],&src[1],&src[2]);
    rotate_z(gamma,det[0],det[1],det[2],&det[0],&det[1],&det[2]);
    rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&pix_u[0],&pix_u[1],&pix_u[2]);
    rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&pix_v[0],&pix_v[1],&pix_v[2]);

    // rotate by projection angle around Z
    angles = create_angles(0,2*M_PI,nproj);
    vectors = new double[12*nproj];
    for(i=0;i<nproj;i++) {
        gamma = angles[i];
        rotate_z(gamma,src[0],src[1],src[2],&vectors[12*i+0],&vectors[12*i+1],&vectors[12*i+2]);
        rotate_z(gamma,det[0],det[1],det[2],&vectors[12*i+3],&vectors[12*i+4],&vectors[12*i+5]);
        rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&vectors[12*i+6],&vectors[12*i+7],&vectors[12*i+8]);
        rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&vectors[12*i+9],&vectors[12*i+10],&vectors[12*i+11]);
    }

    // create projection geometry
    proj_geom = create_proj_geom_3d_cone_vec(sino_axial,det_perp,nproj,vectors);
    delete[] vectors;

    // create CUDA 3D projector
    proj = create_projector_3d_cuda(proj_geom,vol_geom);

    // create sinogram data
    sino = create_data_3d_sino(proj_geom,sino_full_f);
    delete[] sino_full_f;

    // create reconstruction data
    rec = create_data_3d_vol(vol_geom,0.0);

    // create and run algorithm
    algo = new CCudaSirtAlgorithm3D();
    algo->initialize(proj,sino,rec);
    algo->setConstraints(true,0.0,false,0.0);
    algo->run(150);
    algo->getResidualNorm(norm);
    delete algo;

    cout << norm << '\n';

    // write reconstruction data
    save_data_3d("../../../../reconstruction_full.dat",rec);

    delete vol_geom;
    delete[] angles;
    delete proj_geom;
    delete proj;
    delete sino;
    delete rec;
    
    return 0;
}
